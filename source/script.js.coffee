###
  @licstart  The following is the entire license notice for the JavaScript code in this page.

  simple carousel, easily add a carousel on your web pages

      Copyright (C) 2013  acoeuro

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  @licend  The above is the entire license notice for the JavaScript code in this page.
###

# Detect css3 transitions
style = document.documentElement.style
csstransitions =
  style.webkitTransition != undefined ||
  style.MozTransition != undefined ||
  style.OTransition != undefined ||
  style.MsTransition != undefined ||
  style.transition != undefined

$ ->
  $('.simpleCarousel').each ->
    (elt = $(this))
      # Loop index
      .data('slideIndex', 0)
      # Default carousel delay
      .data('delay', elt.data('delay') ? 5000)
      # So that last pane is first pane clone
      .append(elt.children().first().clone())
      # Viewport necessary to "clip" the panels
      .wrap('<div class="simpleCarouselViewport" />')
      .parent()
      # Add the next and previous buttons to the parent viewport
      .before($('<span class="prev">&lt</span>').click -> slide elt, -1)
      .after($('<span class="next">&gt</span>').click -> slide elt, 1)
      .children().click (event) ->
        if ($(event.target).closest('a, pre, code').size() == 0)
          slide elt, 1

    # Remove spaces between slides
    elt.contents().filter ->
      this.nodeType == 3
    .remove()

    timer elt

timer = (elt) ->
  if (elt.data('delay') > 0)
    elt.data 'timeOut', setTimeout ->
      slide elt
    , elt.data 'delay'

slide = (elt, direction = 0) ->
  if direction == 0
    # Stop movement when mouse is hovering
    if elt.parent().is(':hover') or elt.parent().prev().is(':hover') or elt.parent().next().is(':hover')
      timer elt
      return
    direction = 1
  else
    clearTimeout elt.data 'timeOut'

  elt.data 'slideIndex', elt.data('slideIndex') + direction
  #console.log "Slide: "+elt.data('slideIndex')

  if 0 <= elt.data('slideIndex') < elt.children().size()
    margin = "#{-100 * elt.data 'slideIndex'}%"
    if csstransitions
      elt.addClass('animate').css 'margin-left', margin
    else
      # Necessary for older ie
      elt.parent().width(elt.parent().parent().width())
      elt.animate { marginLeft: -1*elt.data('slideIndex')*elt.width() }
    timer elt

  else
    if direction > 0
      elt.data 'slideIndex', 0
    else
      elt.data 'slideIndex', elt.children().size() - 1

    elt.removeClass('animate')
      .css 'margin-left', "#{-100 * elt.data 'slideIndex'}%", ->
      elt.css 'marginLeft' # Flush the css
      slide elt, direction
